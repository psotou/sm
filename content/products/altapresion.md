{
    "title": "Alta presion con filtro",
    "date": "2018-05-12T15:48:00+05:30",
    "tags": [""],
    "categories": ["Boquillas"],
    "images": ["img/altapresion/PSU_0385.jpg", "img/altapresion/PSU_0422.jpg", "img/altapresion/PSU_0424.jpg", "img/altapresion/PSU_0425.jpg", "img/altapresion/PSU_0426.jpg"],
    "thumbnailImage": "img/altapresion/PSU_0424.jpg",
    "actualPrice": "-",
    "comparePrice": null,
    "inStock": true,
    "options": {},
    "variants": []
}
<!-- 
Total Fixture Dimensions (in inches): 51x51x63

Material: Aluminium

Colour: Black Gold

Pack Content: 1 piece lamp with all parts included. Bulbs not included.

Bulb Recommended: E14 CFL, LED or Incandescent Bulb -->