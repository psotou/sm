{
    "title": "Niebla seca",
    "date": "2018-05-12T15:48:00+05:30",
    "tags": [""],
    "categories": ["Boquillas"],
    "images": ["img/niebla-seca/PSU_0404.jpg", "img/niebla-seca/PSU_0408.jpg", "img/niebla-seca/PSU_0413.jpg"],
    "thumbnailImage": "img/niebla-seca/PSU_0404.jpg",
    "actualPrice": "-",
    "comparePrice": null,
    "inStock": true,
    "options": {},
    "variants": []
}

<!-- Total Fixture Dimensions (in inches): 51x51x63

Material: Aluminium

Colour: Black Gold

Pack Content: 1 piece lamp with all parts included. Bulbs not included.

Bulb Recommended: E14 CFL, LED or Incandescent Bulb -->